/*
 * Frame.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef FRAME_H_
#define FRAME_H_

class Frame {
public:
	enum frameType {
		hardtail, fullSuspention
	};
	Frame(frameType type = fullSuspention, int size = 17);

	int getFrameSize() const {
		return frameSize;
	}

	void setFrameSize(int frameSize) {
		this->frameSize = frameSize;
	}

	frameType getType() const {
		return fType;
	}

	void setType(frameType type) {
		fType = type;
	}

private:
	int frameSize;
	frameType fType;
};

#endif /* FRAME_H_ */
