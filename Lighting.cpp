/*
 * Lighting.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "Lighting.h"

Lighting::Lighting(Lighting::lightType lType, bool nIsOn) {
	type = lType;
	on =nIsOn;
}

void Lighting::turnOnFrontLight() {
	if (type == front)
		on = 1;
}
void Lighting::turnOffFrontLight() {
	if (type == front)
		on = 0;
}
void Lighting::turnOnRearLight() {
	if (type == rear)
		on = 1;
}
void Lighting::turnOffRearLight() {
	if (type == rear)
		on = 0;
}

//Frame::Frame(Frame::frameType fType): type(fType){ // example of enum use
//	if (fType == Frame::frameType::hardtail)
//		cout << "Hartail!" << endl;
//}
