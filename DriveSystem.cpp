/*
 * DriveSystem.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "DriveSystem.h"

DriveSystem::DriveSystem(DriveSystem::gearType type, int gearsNr, DriveSystem::pedalsType pedalsType, int current) {
	gearsNumber = gearsNr;
	currentGear = current;
	gType = type;
	pType = pedalsType;
}
void DriveSystem::shiftGearUp(int number) {
	currentGear += number;
	if (currentGear > gearsNumber)
		currentGear = gearsNumber;
}
void DriveSystem::shiftGearDown(int number) {
	currentGear -= number;
	if (currentGear < 1)
		currentGear = 1;
}
