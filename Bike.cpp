/*
 * bike.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "Bike.h"

#include <iostream>
using std::cout;
using std::endl;

//Bike::Bike(){
//}
Bike::Bike() {
	setBarType(sports);
	setForkType(air);
	setFrontBrakeType(disc);
	setRearBrakeType(disc);
	setSeatType(racing);
	frontGear.setGearsNumber(3);
	rearGear.setGType(DriveSystem::rear);
	rearGear.setGearsNumber(9);
	rearLight.setType(Lighting::rear);
}
Bike::Bike(Bike::forkType forkType, Frame::frameType frameType, int frameSize,
		Wheel::tireType tireType, int wheelSize, Bike::barType barType,
		Bike::seatType seatType, Bike::brakeType brakeType, int frontGearsNr,
		int rearGearsNr, DriveSystem::pedalsType pedalsType) :
		forkTypeV(forkType), frame(frameType, frameSize), frontWheel(tireType,
				wheelSize), rearWheel(tireType, wheelSize), barTypeV(barType),
				seatTypeV(seatType), frontBrakeType(brakeType), rearBrakeType(brakeType),
				frontGear(DriveSystem::front, frontGearsNr, pedalsType),
				rearGear(DriveSystem::rear, rearGearsNr), rearLight(Lighting::rear) {
}
//Bike::Bike (Frame::frameType frameType):frame(frameType)		//	important (example)!!!!!



void Bike::printBike() {
	cout << "Fork type: " << getForkType() << endl;
	cout << "Bar type: " << getBarType() << endl;
	cout << "Seat type: " << getSeatType() << endl;
	cout << "Front brake type: " << getFrontBrakeType() << endl;
	cout << "Rear brake type: " << getRearBrakeType() << endl;
	cout << "Pedals type: " << frontGear.getPType() << endl;
	cout << "Frame: " << endl;
	cout << "\t" << "Frame size: " << frame.getFrameSize() << endl;
	cout << "\t" << "Frame type: " << frame.getType() << endl;
	cout << "Front wheel: " << endl;
	cout << "\t" << "Wheel size: " << frontWheel.getWheelSize() << endl;
	cout << "\t" << "Tire type: " << frontWheel.getType() << endl;
	cout << "Rear wheel: " << endl;
	cout << "\t" << "Wheel size: " << rearWheel.getWheelSize() << endl;
	cout << "\t" << "Tire type: " << rearWheel.getType() << endl;
	cout << "Front gear: " << endl;
	cout << "\t" << "Gear type: " << frontGear.getGType() << endl;
	cout << "\t" << "Gears number: " << frontGear.getGearsNumber() << endl;
	cout << "\t" << "Current gear: " << frontGear.getCurrentGear() << endl;
	cout << "Rear gear: " << endl;
	cout << "\t" << "Gear type: " << rearGear.getGType() << endl;
	cout << "\t" << "Gears number: " << rearGear.getGearsNumber() << endl;
	cout << "\t" << "Current gear: " << rearGear.getCurrentGear() << endl;
	cout << "Front light: " << endl;
	cout << "\t" << "Light type: " << frontLight.getType() << endl;
	cout << "\t" << "Is light on? " << frontLight.isOn() << endl;
	cout << "Rear light: " << endl;
	cout << "\t" << "Light type: " << rearLight.getType() << endl;
	cout << "\t" << "Is light on? " << rearLight.isOn() << endl;
}
void Bike::turnOnFrontLight() {frontLight.setIsOn(1);}
void Bike::turnOffFrontLight() {frontLight.setIsOn(0);}
void Bike::turnOnRearLight() {rearLight.setIsOn(1);}
void Bike::turnOffRearLight() {rearLight.setIsOn(0);}
void Bike::shiftFrontGearUp(int number) {frontGear.shiftGearUp(number);}
void Bike::shiftFrontGearDown(int number) {frontGear.shiftGearDown(number);}
void Bike::shiftRearGearUp(int number) {rearGear.shiftGearUp(number);}
void Bike::shiftRearGearDown(int number) {rearGear.shiftGearDown(number);}
void Bike::brake (bool frontBrake, bool rearBrake){
	if (frontBrake&&rearBrake)
		cout << "Using both brakes!" << endl;
	else if(frontBrake)
		cout << "Using front brake, lean back!" << endl;
	else if (rearBrake)
		cout << "Using rear brake!" << endl;
}
