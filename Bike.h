/*
 * bike.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef BIKE_H_
#define BIKE_H_

#include <iostream>
#include "Frame.h"
#include "Wheel.h"
#include "DriveSystem.h"
#include "Lighting.h"
using std::cout;
using std::endl;

class Bike {
public:
	enum forkType {spring, oil, air};
	enum barType {sports, turistic};
	enum seatType {racing, comfortable};
	enum brakeType{disc, vBrake, uBrake};
	Bike();
	Bike(Bike::forkType forkType, Frame::frameType frameType, int frameSize,
			Wheel::tireType tireType, int wheelSize, Bike::barType barType,
			Bike::seatType seatType, Bike::brakeType brakeType,
			int frontGearsNr, int rearGearsNr,
			DriveSystem::pedalsType pedalsType);

	void ride() {cout << "Riding fast!" << endl;}
	void turnLeft() {cout << "Turning left!" << endl;}
	void turnRight() {cout << "Turning right!" << endl;}
	void brake (bool frontBrake=0, bool boolrearBrake=1);

	void printBike();
	void turnOnFrontLight();
	void turnOffFrontLight();
	void turnOnRearLight();
	void turnOffRearLight();

	void shiftFrontGearUp(int number = 1);
	void shiftFrontGearDown(int number = 1);
	void shiftRearGearUp(int number = 1);
	void shiftRearGearDown(int number = 1);

	const Frame& getFrame() const {return frame;}
	void setFrame(const Frame& frame) {this->frame = frame;}
	const DriveSystem& getFrontGear() const {return frontGear;}
	void setFrontGear(const DriveSystem& frontGear) {this->frontGear = frontGear;}
	const Lighting& getFrontLight() const {return frontLight;}
	void setFrontLight(const Lighting& frontLight) {this->frontLight = frontLight;}
	const Wheel& getFrontWheel() const {return frontWheel;}
	void setFrontWheel(const Wheel& frontWheel) {this->frontWheel = frontWheel;}
	const DriveSystem& getRearGear() const {return rearGear;}
	void setRearGear(const DriveSystem& rearGear) {this->rearGear = rearGear;}
	const Lighting& getRearLight() const {return rearLight;}
	void setRearLight(const Lighting& rearLight) {this->rearLight = rearLight;}
	const Wheel& getRearWheel() const {return rearWheel;}
	void setRearWheel(const Wheel& rearWheel) {this->rearWheel = rearWheel;}

	barType getBarType() const {return barTypeV;}
	void setBarType(barType barType) {this->barTypeV = barType;}
	forkType getForkType() const {return forkTypeV;}
	void setForkType(forkType forkType) {this->forkTypeV = forkType;}
	brakeType getFrontBrakeType() const {return frontBrakeType;}
	void setFrontBrakeType(brakeType frontBrakeType) {this->frontBrakeType = frontBrakeType;}
	brakeType getRearBrakeType() const {return rearBrakeType;}
	void setRearBrakeType(brakeType rearBrakeType) {this->rearBrakeType = rearBrakeType;}
	seatType getSeatType() const {return seatTypeV;}
	void setSeatType(seatType seatType) {this->seatTypeV = seatType;}

private:
	Frame frame;
	Wheel frontWheel;
	Wheel rearWheel;
	DriveSystem frontGear;
	DriveSystem rearGear;
	Lighting frontLight;
	Lighting rearLight;
	forkType forkTypeV;
	barType barTypeV;
	seatType seatTypeV;
	brakeType frontBrakeType;
	brakeType rearBrakeType;
	//czy da si� jakos obejsc to, zeby tym enum i zmienna tego typu mialy takie same nazwy?
	//np forkType forkType? problem przy wywo�aniu funkcji
};
#endif /* BIKE_H_ */
