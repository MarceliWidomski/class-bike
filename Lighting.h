/*
 * Lighting.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef LIGHTING_H_
#define LIGHTING_H_

class Lighting {
public:
	enum lightType {front, rear};
	Lighting(lightType lType = front, bool nIsOn = 0 );
	void turnOnFrontLight();
	void turnOnRearLight();
	void turnOffFrontLight();
	void turnOffRearLight();

	bool isOn() const {return on;}
	void setIsOn(bool isOn) {this->on = isOn;}
	lightType getType() const {return type;}
	void setType(lightType type) {this->type = type;}

private:
	bool on;
	lightType type;
};
#endif /* LIGHTING_H_ */
