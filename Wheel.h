/*
 * Wheel.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef WHEEL_H_
#define WHEEL_H_

class Wheel {
public:
	enum tireType {MTB, road, city};
	Wheel(Wheel::tireType type = MTB, int size = 26);

	tireType getType() const {
		return tType;
	}

	void setType(tireType type) {
		tType = type;
	}

	int getWheelSize() const {
		return wheelSize;
	}

	void setWheelSize(int wheelSize) {
		this->wheelSize = wheelSize;
	}

private:
	int wheelSize;
	tireType tType;
};

#endif /* WHEEL_H_ */
