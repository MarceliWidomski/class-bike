//============================================================================
// Name        : bikes.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "Bike.h"
using namespace std;

int main() {

	Bike myBike;
	myBike.turnOnFrontLight();
	myBike.turnOnRearLight();
	myBike.shiftFrontGearUp(3);
	myBike.shiftRearGearUp(8);
	myBike.shiftRearGearDown();
	myBike.printBike();

	myBike.ride();
	myBike.turnLeft();

	Bike customBike(Bike::oil, Frame::hardtail, 20, Wheel::city, 28,
			Bike::sports, Bike::racing, Bike::disc, 3, 10,
			DriveSystem::clipless);
	customBike.printBike();
	customBike.brake(1,0);
	return 0;
}
