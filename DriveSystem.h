/*
 * DriveSystem.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef DRIVESYSTEM_H_
#define DRIVESYSTEM_H_

class DriveSystem {
public:
	enum pedalsType {flat, clipless};
	enum gearType {front, rear};
	DriveSystem(gearType type = front, int gearsNr=1, pedalsType pType = flat, int current=1);
	void shiftGearUp(int number);
	void shiftGearDown(int number);

	int getCurrentGear() const {return currentGear;}
	void setCurrentGear(int currentGear) {this->currentGear = currentGear;}
	int getGearsNumber() const {return gearsNumber;}
	void setGearsNumber(int gearsNumber) {this->gearsNumber = gearsNumber;}

	gearType getGType() const {return gType;}
	void setGType(gearType type) {gType = type;}
	pedalsType getPType() const {return pType;}
	void setPType(pedalsType type) {pType = type;}

private:
	int gearsNumber;
	int currentGear;
	gearType gType;
	pedalsType pType;
};

#endif /* DRIVESYSTEM_H_ */
